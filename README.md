# Ansible Role Linux Gitlab Server

## General Information

**Author:**     Jesús Alejandro Sánchez Dávila

**Maintainer:** Jesús Alejandro Sánchez Dávila

**Email:**      [jsanchez.consultant@gmail.com](mailto:jsanchez.consultant@gmail.com)

## Description

This role installs Apache HTTP Server

This role was created with `molecule` with the Docker driver to allow for pre-testing. It can be added/installed via ansible-galaxy to make use of it as a building block from complex playbooks.

## Supported Platforms

This role has been tested on:

- _Alma Linux 8_
- _Amazon Linux 2_
- _Amazon Linux 2023_
- _Debian 11_
- _CEntOS 7_
- _Opensuse Leap 15_
- _Rocky Linux 8_
- _Ubuntu 20.04_
- _Ubuntu 22.04_

### Notes

#### Amazon Linux

- No firewall is configured
- EPEL Repository is not available for Amazon Linux 2023
- Cockpit Project is not available for any Amazon Linxu version

#### CentOS

- FirewallD and Iptables testing is disabled due to issues with the packages in the CentOS 7 Docker container

#### Ubuntu

- UFW and iptables testing is disabled due to issues with package versions and availability in Docker containers for 20.04 and 22.04

## Usage

### Role Requirements File

This role can be added to a `requirements.yml` file as follows

    # Silvarion Linux Apache HTTP Server Role
    - src: git@gitlab.com:silvarion/ansible/roles/lnx_gitlab.git
      scm: git
      version: "main"  # quoted, so YAML doesn't parse this as a floating-point value

Then run `ansible-galaxy install -r requirements.yml`

### Playbook Additional Task

An addirional task can also be added in a playbook to install the role locally before including the role in the playbook itself

    ---
    - name: Sample Playbook
      hosts: all
      gather_facts: true
      # ...

      collections:
        # All the collections you'll need

      pre_tasks:
        - name: Install requirements
          delegate_to: localhost
          ansible.builtin.command: ansible-galaxy install -r {{ role_path }}/requirements.yml # <-- This is your requirements.yml file
        # Other tasks that you run BEFORE the roles

      roles:
        - { role: "lnx_gitlab" }
        # your other roles

      post_tasks:
        # other tasks that you run AFTER the roles

### Manual Installation
